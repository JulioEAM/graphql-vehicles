# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

10.times do |index|
  style_row = BodyStyle.new(description: Faker::Vehicle.car_type)
  style_row.save
  fuel_row = FuelType.new(description: Faker::Vehicle.fuel_type)
  fuel_row.save
  type_row = Type.new(description: Faker::Vehicle.style)
  type_row.save
  consignor_row = Consignor.new(name: Faker::Movies::BackToTheFuture.character)
  consignor_row.save
  auction_row = Auction.new(name: Faker::Music::RockBand.name)
  auction_row.save
  make_row = Make.new(description: Faker::Vehicle.make)
  make_row.save
end

MAKES = Make.all

MAKES.each do |make|
  modelses = 15.times.map { Faker::Vehicle.model(make_of_model: make[:description]) }.uniq
  modelses.each do |model|
    make.models.create(description: model, make_id: make[:id])
  end
end

MODELS = Model.all

STYLES = BodyStyle.all

FUELS = FuelType.all

TYPES = Type.all

CONSIGNORS = Consignor.all

AUCTIONS = Auction.all

MODELS.each do |model|
  vehicle_description = Faker::Vehicle.make_and_model
  split_for_trim = vehicle_description.split(' ')
  trim = split_for_trim[0][0..1] + ' ' + split_for_trim[1][0..1]
  trim_row = Trim.create(description: trim)
  vehicle = Vehicle.new(
    description: vehicle_description,
    make_id: model[:make_id],
    model_id: model[:id],
    trim_id: trim_row[:id],
    body_style_id: STYLES.sample[:id],
    fuel_type_id: FUELS.sample[:id],
    type_id: TYPES.sample[:id],
    consignor_id: CONSIGNORS.sample[:id],
    auction_id: AUCTIONS.sample[:id]
  )
  vehicle.save
end