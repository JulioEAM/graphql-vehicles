class CreateBodyStyles < ActiveRecord::Migration[6.0]
  def change
    create_table :body_styles do |t|
      t.string :description

      t.timestamps
    end
  end
end
