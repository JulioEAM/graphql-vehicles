class CreateVehicles < ActiveRecord::Migration[6.0]
  def change
    create_table :vehicles do |t|
      t.string :description
      t.belongs_to :make, null: false, foreign_key: true
      t.belongs_to :model, null: false, foreign_key: true
      t.belongs_to :trim, null: false, foreign_key: true
      t.belongs_to :body_style, null: false, foreign_key: true
      t.belongs_to :fuel_type, null: false, foreign_key: true
      t.belongs_to :type, null: false, foreign_key: true
      t.belongs_to :consignor, null: false, foreign_key: true
      t.belongs_to :auction, null: false, foreign_key: true

      t.timestamps
    end
  end
end
