class Vehicle < ApplicationRecord
  belongs_to :make
  belongs_to :model
  belongs_to :trim
  belongs_to :body_style
  belongs_to :fuel_type
  belongs_to :type
  belongs_to :consignor
  belongs_to :auction
end
